from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from lists.views import home_page
from django.template.loader import render_to_string
from lists.models import Item,List


class SmokeTest(TestCase):
    '''тест на токсичность'''
    def test_bad_maths(self):
        self.assertEqual(1+2,3)


class ListAndItemModelTest(TestCase):
    '''тест модели элемента списка'''
    def test_saving_and_retrieving_items(self):
        list_ = List()
        list_.save()

        first_item = Item()
        first_item.text = 'The first (ever) list item'
        first_item.list = list_
        first_item.save()

        second_item = Item()
        second_item.text = 'Item the second'
        second_item.list = list_
        second_item.save()

        saved_list = List.objects.first()
        self.assertEqual(saved_list, list_)

        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(),2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.text, 'Первый (самый) элемент списка')
        self.assertEqual(first_saved_item, list_)
        self.assertEqual(second_saved_item.text, 'Элемент второй')
        self.assertEqual(second_saved_item, list_)




class HomePageTest(TestCase):
    def test_displays_all_list_items(self):
        '''тест отображаются все элементы списка'''
        Item.objects.create(text='itemey 1')
        Item.objects.create(text='itemey 2')

        response = self.client.get('/')

        self.assertIn('itemey 1', response.content.decode())
        self.assertIn('itemey 2', response.content.decode())

    def test_only_saves_items_when_necessary(self):
        '''тест сохранять элементы когда нужно'''
        self.client.get('/')
        self.assertEqual(Item.objects.count(),0)
    '''тест домашней страницы'''
    def test_root_url_resolves_to_home_page_view(self):
        '''тест корневой url преобразуется в представление домашней страницы'''
        found = resolve('/')
        self.assertEqual(found.func, home_page)
    def test_can_save_a_POST_request(self):
        '''тест можно сохранить post-запрос'''
        responce = self.client.post('/',data = {'item_text':'A new list item'})
        self.assertEqual(Item.objects.count(),1)

        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new list item')
    def test_redirects_after_POST(self):
        '''переадресует POST запрос'''
        response = self.client.post('/',data = {'item_text':'A new list item'})
        self.assertEqual(response.status_code,302)
        self.assertEqual(response['location'],'/lists/единственный-в своем-роде-список-в-мире/')

        #self.assertIn('A new list item', responce.content.decode())
        #self.assertTemplateUsed(responce,'home.html')
    def test_users_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_returns_correct_html(self):
        '''тест: домашняя страница возвращает правильный html'''
        #request = HttpRequest()
        #response = home_page(request)
        response = self.client.get('/')
        html = response.content.decode('utf8')
        self.assertTrue(html.startswith('<html>'))
        self.assertIn('<title>To-Do lists</title>',html)
        self.assertTrue(html.endswith('</html>'))

        self.assertTemplateUsed(response, 'home.html')
        #excepted_html = render_to_string('home.html')
        #self.assertEqual(html, excepted_html)

        #self.assertTrue(html.startswith('<html>'))
        #self.assertIn('<title>To-Do lists</title>',html)
        #self.assertTrue(html.endswith('</html>'))

class ListViewTest(TestCase):
    '''тест представление списка'''
    def test_displays_all_items(self):
        '''тест: отображаются все элементы списка'''
        list_ = List.objects.create()
        Item.objects.create(text = 'itemey 1',list = list_)
        Item.objects.create(text ='itemey 2',list = list_)

        response = self.client.get('/lists/один-единственный-список-в-мире/')

        self.assertContains(response, 'itemey 1')
        self.assertContains(response, 'itemey 2')

    def test_uses_list_template(self):
        '''тест: используется шаблон списка'''
        list_ = List.objects.create()
        response = self.client.get(f'/lists/{list_.id}/')
        self.assertTemplateUsed(response,'list.html')

    def test_display_all_items(self):
        '''тест: отображаются элементы только для этого списка'''
        correct_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=correct_list)
        Item.objects.create(text='itemey 2', list=correct_list)

        other_list = List.objects.create()
        Item.objects.create(text='другой элемент 1 списка', list=other_list)
        Item.objects.create(text='другой элемент 2 списка', list=other_list)

        response = self.client.get(f'/lists/{correct_list.id}/')

        self.assertContains(response, 'itemey 1')
        self.assertContains(response, 'itemey 2')
        self.assertContains(response, 'другой элемент 1 списка')
        self.assertContains(response, 'другой элемент 2 списка')

    def test_passes_correct_list_to_template(self):
        '''тест: передается правильный шаблон списка'''
        other_list = List.objects.create()
        correct_list = List.objects.create()
        response = self.client.get(f'/lists/{correct_list.id}/')
        self.assertEqual(response.context['list'],correct_list)


class NewListTest(TestCase):
    '''тест нового списка'''

    def test_can_save_a_POST_request(self):
        '''тест: можно сохранить post-запрос'''
        self.client.post('/lists/new', data = {'item_text': 'A new list item'})
        self.assertEqual(Item.objects.count(),1)
        new_item = Item.objects.first()
        self.assertEqual(new_item, 'A new list item')

    def test_redirects_after_POST(self):
        '''тест: переадресует после post-запроса'''
        response = self.client.post('/lists/new',data={'item_text': 'A new list item'})
        new_list = List.objects.first()
        self.assertRedirects(response,f'/lists/{new_list.id}/')
        #self.assertEqual(response.status_code, 302)
        #self.assertEqual(response['location'], '/lists/единственный-список-в-мире/')

    def test_can_save_a_POST_request_to_an_existing_list(self):
        '''тест: можно сохранять существующий запрос в существующий список'''
        other_list = List.objects.create()
        correct_list = List.objects.creatte()

        self.client.post(
            f'/lists/{correct_list.id}/add_item',
            data = {'iten_text': 'A new item for an existing list'}
        )
        self.assertEqual(Item.objects.count(),1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new item for an existing list')
        self.assertEqual(new_item.list, correct_list)

    def test_redirects_to_list_view(self):
        '''тест: переадресуется в предстваление списка'''
        other_list = List.objects.create()
        correct_list = List.objects.create()

        response = self.client.post(
            f'/lists/{correct_list.id}/add_item',
            data = {'iten_text': 'A new item for an existing list'}
        )

        self.assertRedirects(response, f'/lists/{correct_list.id}/')