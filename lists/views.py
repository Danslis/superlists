from django.shortcuts import redirect,render
from lists.models import Item,List
from django.core.exceptions import ValidationError
from lists.forms import ItemForm
from django.http import HttpResponse
#home_page = None

# Create your views here.

def home_page(request):
    '''Домашняя страница'''
    #if request.method == 'POST':
       #Item.objects.create(text = request.POST['item_text'])
       # return redirect('/lists/единственный-в своем-роде-список-в-мире/')
    #items = Item.objects.all()
    return render(request, 'home.html', {'form': ItemForm()})
    #if request.method == 'POST':
        #return HttpResponse(request.POST['item_text'])
    #item = Item()
    #item.text = request.POST.get('item_text', '')
    #item.save()


    #return render(request, 'home.html',{
        #'new_item_text':item.text
    #})


def view_list(request,list_id):
    '''представление списка'''
    list_ = List.objects.get(id=list_id)
    #error = None
    form = ItemForm()
    if request.method == 'POST':
        form = ItemForm(data=request.POST)
        if form.is_valid():
            Item.objects.create(text=request.POST['text'],list=list_)
            return redirect(list_)
        #try:
            #item=Item(text=request.POST['item_text'],list=list_)
            #item.full_clean()
            #item.save()
            #
        #except ValidationError:
            #error = "You can't have an empty list item"
    #form= ItemForm()
    return render(request, 'list.html',{'list':list_,'form':form})

def new_list(request):
    '''новый список'''
    #list_ = List.objects.create()
    #item = Item.objects.create(text=request.POST['item_text'], list=list_)
    #return redirect('/lists/the-only-list-in-the-world/')
    form = ItemForm(data=request.POST)
    if form.is_valid():
        list_ = List.objects.create()
        Item.objects.create(text=request.POST['text'], list=list_)
        return redirect(list_)
    #try:
        #item.full_clean()
        item.save()
    #except ValidationError:
        #list_.delete()
        #error = "You can't have an empty list item"
    else:
        return render(request, 'home.html', {"form":form})
    #return redirect(list_)

#def add_item(request, list_id):
    #list_ = List.objects.get(id = list_id)
    #Item.objects.create(text=request.POST['item_text'], list=list_)
    #return redirect(f'/lists/{list_.id}/')