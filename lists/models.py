from django.db import models
from django.urls import reverse
# Create your models here.
class List(models.Model):
    '''Список'''
    #list = models.TextField(default='')
    def get_absolute_url(self):
        '''получить абсолютный url'''
        return reverse('view_list',args=[self.id])
class Item (models.Model):
    '''Элемент списка'''
    text = models.TextField(default='')
    #list = models.ForeignKey(List, default=None)
    list = models.ForeignKey(List, default='None', on_delete="")